const http = require("http"), URL = require("url").URL;
http.createServer(function (request, response) {
    var items = ["milk", "bread", "eggs", "flour"];

    const parameters = new URL(request.url, "http://localhost")
        .searchParams;

    var item_num = parameters.get("item_num");

    response.writeHead(200, {
        "Content-Type": "text/plain"
    });

    response.end(items[item_num]);
}).listen(8081);
