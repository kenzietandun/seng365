const express = require('express');
const app = express();

const bodyParser = require('body-parser');
app.use(bodyParser.json());

const data = require('./users.json');
const users = data.users;
const posts = data.posts;

app.get('/users', function (req, res) {
    res.send(users);
});

app.get('/users/:id', function (req, res) {
    let id = req.params.id;
    let res_data = "No user";

    for (let user of users) {
        if (id == user.id) {
            res_data = user;
            break;
        }
    }

    res.send(res_data);
});

app.post('/users', function (req, res) {
    let user_data = req.body;

    users.push(user_data);
    res.send(users);
});

app.put('/users/:id', function (req, res) {
    let id = req.params.id;
    let user_data = req.body;

    for (let user of users) {
        if (id == user.id) {
            let uid = users.indexOf(user);
            users[uid] = user_data;
            break;
        }
    }

    res.send(user_data);
});

app.delete('/users/:id', function (req, res) {
    let id = req.params.id;

    for (let user of users) {
        if (id == user.id) {
            let uid = users.indexOf(user);
            users.splice(uid, 1);
        }
    }

    res.send(users);
});

app.get('/followings/:id', function (req, res) {
    res.send(users.followings);
});

app.post('/followings/:id', function (req, res) {
    let followings = req.body.following;
    let id = req.params.id;

    for (let user of users) {
        if (id == user.id) {
            let uid = users.indexOf(user);
            users[uid].followings.push.apply(users[uid].followings, followings);
        }
    }

    res.send(users);
});

app.put('/followings/:id', function (req, res) {
    let followings = req.body.following;
    let id = req.params.id;

    for (let user of users) {
        if (id == user.id) {
            let uid = users.indexOf(user);
            users[uid].followings = followings;
        }
    }

    res.send(users);
});

app.delete('/followings/:id', function (req, res) {
    let followingToDel = req.body.following;
    let id = req.params.id;

    for (let user of users) {
        if (id == user.id) {
            let uid = users.indexOf(user);
            let indexFollower = users[uid].followings.indexOf(followingToDel);
            if (indexFollower > -1) {
                users[uid].followings.splice(indexFollower, 1);
            }
        }

    }
    res.send(users);
});

function getPostById(id) {
    for (let post of posts) {
        if (post.id == id) {
            return post;
        }
    }

    return null;
}

function getUserById(id) {
    for (let user of users) {
        if (user.id == id) {
            return user;
        }
    }
    return null;
}

function send404(res) {
    res.status(404).send("404 not found");
}

app.get('/posts/:id', function(req, res) {
    let id = req.params.id;
    user = getUserById(id);
    if (user == null) {
        send404(res);
        return;
    }

    userPostsId = user.posts;

    var userPosts = [];
    for (let userPostId of userPostsId) {
        userPosts.push(getPostById(userPostId));
    }

    res.send(userPosts);
});

app.post('/posts/:id', function(req, res) {
    let id = req.params.id;
    let post = req.body;

    posts.push(post);
    user = getUserById(id);
    if (user == null) {
        send404(res);
        return;
    }
    user.posts.push(post.id);
    res.send(user);
});


app.listen(3000, function () {
    console.log("Listening on port 3000");
});

app.use(function (req, res, next) {
    send404(res);
});
