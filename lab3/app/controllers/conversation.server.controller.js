const Conversation = require('../models/conversation.server.model');

exports.list = async function(req, res) {
    try {
        const result = await Conversation.getAll();
        res.status(200)
            .send(result);
    } catch (err) {
        res.status(500)
            .send(`ERROR getting conversations ${err}`);
    }
};

exports.create = async function(req, res) {
    try {
        const convoName = req.body.convo_name;
        const result = await Conversation.insert(convoName);
        res.status(201)
            .send(`Inserted ${convoName} at id ${result}`);
    } catch (err) {
        res.status(500)
            .send(`ERROR creating conversation ${err}`);
    }
};

exports.read = async function(req, res) {
    try {
        const convoId = req.params.convo_id;
        const result = await Conversation.getOne(convoId);
        res.status(200)
            .send(result);
    } catch (err) {
        res.status(500)
            .send(`ERROR getting single conversation ${err}`);
    }
};

exports.update = async function(req, res) {
    try {
        const convoId = req.params.convo_id;
        const convoName = req.body.convo_name;
        const result = await Conversation.alter(convoId, convoName);
        res.status(200)
            .send(result);
    } catch (err) {
        res.status(500)
            .send(`ERROR updating conversation ${err}`);
    }
}

exports.delete = async function(req, res) {
    try {
        const convoId = req.params.convo_id;
        const result = await Conversation.remove(convoId);
        res.status(200)
            .send(result);
    } catch (err) {
        res.status(500)
            .send(`ERROR deleting conversation ${err}`);
    }
}
