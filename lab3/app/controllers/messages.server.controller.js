const Message = require('../models/message.server.model');

exports.list = async function(req, res) {
    try {
        const convoId = req.params.convo_id;
        const result = await Message.getAll(convoId);
        res.status(200)
            .send(result);
    } catch (err) {
        res.status(500)
            .send(`ERROR getting messages ${err}`);
    }
};

exports.create = async function(req, res) {
    try {
        const convoId = req.params.convo_id;
        const userId = req.body.user_id;
        const message = req.body.message;
        const result = await Message.insert(convoId, userId, message);
        res.status(201)
            .send(`Inserted ${message} at id ${result}`);
    } catch (err) {
        res.status(500)
            .send(`ERROR creating message ${err}`);
    }
};

exports.read = async function(req, res) {
    try {
        const convoId = req.params.convo_id;
        const messageId = req.params.msg_id;
        const result = await Message.getOne(convoId, messageId);
        res.status(200)
            .send(result);
    } catch (err) {
        res.status(500)
            .send(`ERROR getting single message ${err}`);
    }
};
