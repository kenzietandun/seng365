const db = require('../../config/db');

exports.getAll = async function(convoId) {
    const connection = await db.getPool().getConnection();
    const q = 'SELECT * FROM lab2_messages WHERE convo_id = ?';
    const [rows, fields] = await connection.query(q, [convoId]);
    return rows;
};

exports.getOne = async function(convoId, messageId) {
    const connection = await db.getPool().getConnection();
    const q = 'SELECT * FROM lab2_messages WHERE convo_id = ? and message_id = ?';
    const [rows, _] = await connection.query(q, [convoId, messageId]);
    return rows;
};

exports.insert = async function(convoId, userId, message) {
    const connection = await db.getPool().getConnection();
    const q = 'INSERT INTO lab2_messages (convo_id, user_id, message) VALUES (?, ?, ?)';
    const [result, _] = await connection.query(q, [convoId, userId, message]);
    console.log(`Inserted message with id ${result.insertId}`);
    return result.insertId;
};
