const db = require('../../config/db');

exports.getAll = async function() {
    const connection = await db.getPool().getConnection();
    const q = 'SELECT * FROM lab2_conversations';
    const [rows, fields] = await connection.query(q);
    return rows;
};

exports.getOne = async function(conversationId) {
    const connection = await db.getPool().getConnection();
    const q = 'SELECT * FROM lab2_conversations WHERE convo_id = ?';
    const [rows, _] = await connection.query(q, conversationId);
    return rows;
};

exports.insert = async function(conversationName) {
    let values = [conversationName];
    const connection = await db.getPool().getConnection();
    const q = 'INSERT INTO lab2_conversations (convo_name) VALUES (?)';
    const [result, _] = await connection.query(q, values);
    console.log(`Inserted conversation with id ${result.insertId}`);
    return result.insertId;
};

exports.alter = async function(conversationId, conversationName) {
    const connection = await db.getPool().getConnection();
    const q = 'UPDATE lab2_conversations SET convo_name = ? WHERE convo_id = ?';
    const [rows, _] = await connection.query(q, [conversationName, conversationId]);
    return rows;
};

exports.remove = async function(conversationId) {
    const connection = await db.getPool().getConnection();
    const q = 'DELETE FROM lab2_conversations WHERE convo_id = ?';
    const [rows, _] = await connection.query(q, [conversationId]);
    return rows
};
